# PythonTutorial

The tutorial consists of multiple parts.
- Python Basics
- Python Pandas, Numpy, Plotting, Statsmodels 
- Packaging

## Requierements

You must have python3 installed on your computer. Additionally we'll be using a few packages in the tutorial what we still need to install.

### Installation Python
***

Go to the [official python website](https://www.python.org) click on downloads and select your operating system. 
Then go ahead and download the latest stable release. Windows users most likely need the 64 bit version, alternatively you can check your system version in the system information.

Run the downloaded file and make sure you select the add to Path option!

In case you need further assistance please check out this [website](https://realpython.com/installing-python/#step-2-run-the-installer).

**Check the Installation**

After you finished installing python open a console and type `pip`. This commands output should look something like this:
```bash
Usage:   
  pip <command> [options]

Commands:
  install                     Install packages.
  download                    Download packages.
  uninstall                   Uninstall packages.
  freeze                      Output installed packages in requirements format.
  list                        List installed packages.
  show                        Show information about installed packages.
  check                       Verify installed packages have compatible dependencies.
  search                      Search PyPI for packages.
  wheel                       Build wheels from your requirements.
  hash                        Compute hashes of package archives.
  completion                  A helper command used for command completion.
  help                        Show help for commands.

General Options:
  -h, --help                  Show help.
  --isolated                  Run pip in an isolated mode, ignoring environment variables and user configuration.
  -v, --verbose               Give more output. Option is additive, and can be used up to 3 times.
  -V, --version               Show version and exit.
  -q, --quiet                 Give less output. Option is additive, and can be used up to 3 times (corresponding to WARNING, ERROR,
                              and CRITICAL logging levels).
  --log <path>                Path to a verbose appending log.
  --proxy <proxy>             Specify a proxy in the form [user:passwd@]proxy.server:port.
  --retries <retries>         Maximum number of retries each connection should attempt (default 5 times).
  --timeout <sec>             Set the socket timeout (default 15 seconds).
  --exists-action <action>    Default action when a path already exists: (s)witch, (i)gnore, (w)ipe, (b)ackup, (a)bort.
  --trusted-host <hostname>   Mark this host as trusted, even though it does not have valid or any HTTPS.
  --cert <path>               Path to alternate CA bundle.
  --client-cert <path>        Path to SSL client certificate, a single file containing the private key and the certificate in PEM
                              format.
  --cache-dir <dir>           Store the cache data in <dir>.
  --no-cache-dir              Disable the cache.
  --disable-pip-version-check
                              Don't periodically check PyPI to determine whether a new version of pip is available for download.
                              Implied with --no-index.
```
### Install Python Packages
***

We'll be using:
- jupyter notebook -> lightweight IDE
- pandas -> a library for working with data
- numpy -> library for efficient matrix, array,... manipulation
- matplotlib -> standard plotting library
- plotly -> dynamic plots
- cufflinks -> pandas adapter to plotly
- seaborn -> fancy plots out of the box
- sklearn -> machine learning library

To install the packages open a console and run the following command.
(Linux users should use pip3 instead of pip since they do have Python2 and Python3 installed by default.)
```bash
pip install -r requirements.txt
```
They're many dependencies involved, so the installation might take a minute.

## Starting Jupyter Notebook

I choose *jupyter notebook* (*JN*) as the appropriate IDE for this tutorial, as it is light weight, so it's easy to understand and does not need much resources. Additionally *JN* is great for plotting!

One way to start *JN* is:
-  to open a console and navigate to your working directory (using the cd command)
- now you simply type jupyter notebook

A browser tab will open and this is your IDE. If you don't know what to do from this point on forward check out this comprehensive [tutorial](https://realpython.com/jupyter-notebook-introduction/).

### The rest we'll figure out together in the tutorial!

