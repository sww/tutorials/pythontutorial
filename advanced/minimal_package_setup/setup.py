from setuptools import setup

setup(name='example_package',
      version='0.1',
      description='An example on how to set up a package.',
      author='Your Name',
      author_email='Your.Email@domain.blaaa',
      license='MIT',
      packages=['example_package'],
      install_requires=['pandas'], # this will normally be containg depencies from packages link pandas
      zip_safe=False)
