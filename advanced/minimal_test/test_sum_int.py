# import function "add_arguments" from "my_code"
from my_code import add_arguments

# test different scenarios
def test_add_pos_int():
    assert add_arguments(1,2,3,4,5) == 15, "adding pos int wrong result"

def test_add_neg_int():
    assert add_arguments(-1, -2, -3, -4) == -10, "adding neg int wrong result"

def test_add_zero_int():
    assert add_arguments(-1, 0, 1) == 0, "adding neg int wrong result"
