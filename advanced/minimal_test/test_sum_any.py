# import pytest to use pytest.raises
import pytest
# import function "add_arguments" from "my_code"
from my_code import add_arguments

# test different scenarios
def test_add_int_and_other():
    with pytest.raises(TypeError):
        assert add_arguments(1, "a") 
        assert add_arguments([], "a") 
        assert add_arguments(False, "a")
        #...
    print("finished with test_add_int_and_other")

def test_additional():
    assert 1==1
